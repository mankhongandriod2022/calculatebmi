package com.mankhong.calbmi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.mankhong.calbmi.databinding.ActivityMainBinding
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.buttonCalculate.setOnClickListener { calculateBmi() }
        binding.weightEditText.setOnKeyListener{view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.heightEditText.setOnKeyListener{view, keyCode, _ -> handleKeyEvent(view, keyCode)}

    }
    private fun calculateBmi(){
        val editFieldWeight = binding.weightEditText.text.toString()
        val editFieldHeight = binding.heightEditText.text.toString()
        val weight = editFieldWeight.toDoubleOrNull()
        val height = editFieldHeight.toDoubleOrNull()
        if(weight == null || weight == 0.0 || height == null || height == 0.0){
            return
        }
        var bmi = weight/((height/100).pow(2))
        val df = DecimalFormat("#.#")

        var bmiFormat = df.format(bmi).toDouble()
        binding.textBmi.text = getString(R.string.bmiResult , bmiFormat.toString())
        if(bmiFormat < 18.5){
            binding.textLevel.text = "UNDERWEIGHT"
        }else if(bmiFormat == 18.5 || bmiFormat <= 24.9){
            binding.textLevel.text = "NORMAL"
        }else if(bmiFormat == 25.0 || bmiFormat <= 29.9){
            binding.textLevel.text = "OVERWEIGHT"
        }else {
            binding.textLevel.text = "OBESE"
        }
    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}